#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls --namespace aartzy -q | grep aartzy-web )
if [ "$CHECK_WEB" = "aartzy-web" ]
then
    echo "Updating existing aartzy-web . . ."
    helm upgrade aartzy-web \
        --namespace aartzy \
        --reuse-values \
        helm-chart/aartzy-web
fi
